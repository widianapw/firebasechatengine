import time
import firebase_admin
from db_con import *

from firebase_admin import credentials
from firebase_admin import db
cred = credentials.Certificate("fir-mca-f566b-firebase-adminsdk-gbsaw-2b429ef10c.json")
firebase_admin.initialize_app(cred,{
    'databaseURL': 'https://fir-mca-f566b.firebaseio.com/'
})

if __name__ == '__main__':
    while True:
        curDb = conn.cursor()
        selectOutbox = "select *from outbox where status = '0'"
        curDb.execute(selectOutbox)
        dataOutbox = curDb.fetchall()
        conn.commit()
        if curDb.rowcount == 0:
            print("Tidak ada pesan baru")
        else:
            for data in dataOutbox:
                chatRef = db.reference('chat')
                uid = data[1]
                email = data[2]
                message = data[3]
                millis = int(round(time.time() * 1000))
                chatRef.push({
                    'uid': uid,
                    'email': email,
                    'message': message,
                    'message_type': '2',
                    'timestamp': millis
                })
                outboxRef = db.reference('outbox')
                outboxRef.push({
                    'uid': uid,
                    'email': email,
                    'message': message,
                    'message_type': '2',
                    'timestamp': millis
                })

                updateOutbox = "update outbox set status ='1', timestamps = %s where id = %s"%(millis, data[0])
                print("send chat to %s" % email)
                curDb.execute(updateOutbox)
                conn.commit()

        time.sleep(2)